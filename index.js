
fetch('https://jsonplaceholder.typicode.com/todos')
.then((response) => response.json())

.then((data) => {
	console.log(data)

})

fetch('https://jsonplaceholder.typicode.com/todos/1')
.then((response) => response.json())
.then((data) => console.log(data))


fetch('https://jsonplaceholder.typicode.com/todos/1', {
	method: 'PATCH',
	headers: {
		'Content-type': 'application/json'
	},
	body: JSON.stringify({
		title:'updated post',
	})
})


.then((response) => response.json())
.then((data) => console.log(data))
/*let titleArray = data.map(function (element){
	console.log(element)
})*/
	






let http = require('http');
let port = ('4000')

http.createServer((request, response) => {
	

	if(request.url == "https://jsonplaceholder.typicode.com/todos" && request.method == "GET"){
	
		fetch('https://jsonplaceholder.typicode.com/todos')
		.then((response) => response.json())

		.then((data) => {
		console.log(data)
	}) 
		
	} 

	 if (request.url == "https://jsonplaceholder.typicode.com/todos/1" && request.method == "GET"){
	response.writeHead(200, {'Content-Type':'plain/text'})
	fetch('https://jsonplaceholder.typicode.com/todos/1')
	.then((response) => response.json())
	.then((data) => console.log(data))
		response.end('create to do list items')
	}

	if(request.url == "https://jsonplaceholder.typicode.com/todos" && request.method == "POST"){

		fetch('https://jsonplaceholder.typicode.com/todos', {
			method: 'POST',
			headers: {
				'Content-Type': 'application/json'
			},
			body: JSON.stringify({
				title:'New Post',
				body:'Hello, world! This is my new post!',
				userID:1
			})
		})

		.then((response) => response.json())
		.then((data) => console.log(data))

	}

		if(request.url == "https://jsonplaceholder.typicode.com/todos" && request.method == "PUT"){

			fetch('https://jsonplaceholder.typicode.com/todos', {
				method: 'PUT',
				headers: {
					'Content-type': 'application/json'
				},
				body: JSON.stringify({
					title:'Corrected Post',
				})
			})
		}



	if(request.url == "https://jsonplaceholder.typicode.com/todos" && request.method == "PATCH"){
	
	fetch('https://jsonplaceholder.typicode.com/todos', {
		method: 'PATCH',
		headers: {
			'Content-type': 'application/json'
		},
		body: JSON.stringify({
			title:'Corrected Post',
		})
	})

	.then((response) => response.json())
	.then((data) => console.log(data))


	}

	if(request.url == 'https://jsonplaceholder.typicode.com/todos/1' && request.method == 'DELETE'){
		fetch('https://jsonplaceholder.typicode.com/todos/1')
		.then((response) => response.json())
		.then((data) => console.log(data))
	}

}).listen(port)

console.log(`Server is running at localhost:${port}`)




